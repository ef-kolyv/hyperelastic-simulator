# Invoke NASTRAN

## Why 
1. take a look at the low level implementation 
2. compare the results with the python code (stress)
3. [optional: use it to run the simulation iteratively/ programmatically]
   - note: the output created by the `.dat` file is a bit wordy; if you need a more concise output, create a punch file

## How  

### Modify the FEMAP file
- Modify the Case Control Commands section (after Exec. Control (`CEND`) and before the `BEGIN BULK` see Fig.3-1 in User NASTRAN) to include:  
```
DISPLACEMENT = ALL
FORCE = ALL
STRESS = ALL
```

or use `DISPLACEMENT(PUNCH,REAL) = ALL` for output to a punch file.


### Run the NASTRAN command
Invoke the nastran command (windows wrapper)- under the Siemens installation directory like this: 
```
 & "C:\Program Files\Siemens\Femap 2020.2 Student\nastran\bin\nastranw.exe" .\filename.dat out=result_directory
```

## References

1. NASTRAN User Guide: on how to read the .dat file and the output 
2. NASTRAN Quick Reference Guide: search for the syntax of each command
3. NASTRAN Getting started tutorials: use it as an example of the structure of the file 
