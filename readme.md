# Hyperelastic Simulator

An exercise to investigate Nonlinear Finite Element Analysis with Python and NASTRAN. Provides the code for the 
simulation of a hexahedral element for a linear elastic and a hyperelastic material. The main objective was to map 
the underlying concepts from Continuum Mechanics theory to the computational aspects of its implementation. 
Heavily based on the Matlab code from Kim's book [Introduction to Nonlinear Analysis](https://mae.ufl.edu/nkim/INFEM/) - it is actually a translation to Python and a refactoring of the original code towards a more 
object-oriented style and human-readable naming of the variables.    

## Dependencies 

Written in Python 3.9, using Numpy 1.21.4 and Scipy 1.7.2, NX-NASTRAN

## What it is not

Robust code for nonlinear material simulation; consider using NASTRAN for this purpose. 


## Suggested reading

Things (code structure, naming of variables) make sense by first taking a look at these sources (books):

- [Introduction to Nonlinear Analysis](https://mae.ufl.edu/nkim/INFEM/): the structure and concepts are drawn from 
  this book, but it is more than worth to go through the following first  
1. [A First Course in Continuum Mechanics](https://www.cambridge.org/core/books/first-course-in-continuum-mechanics/DC9A87155531958AD5EFC66AEB981DAE): by Oscar Gonzalez
2. [Nonlinear Continuum Mechanics for Finite Element Analysis](https://www.cambridge.org/core/books/nonlinear-continuum-mechanics-for-finite-element-analysis/67AD6DBAAB77E755C09E7FB82565DA0B): by Javier Bonet, Richard D. Wood

