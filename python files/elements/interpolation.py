import numpy as np


def element_interpolation(xi_vec: np.ndarray, element_spatial: np.ndarray) -> list:
    r"""
    realizes equations: 1.136, 1.137, 1.138
    calculates shape functions, their derivatives and determinant of the Jacobian of 8x node element;
    internal values: jacobian: 3x3 Jacobian
    :node_reference: the plus-minus ones of the node coordinates in the reference frame
    :param xi_vec: 3x1
    vector of reference coordinates (\xi, \eta, \zeta): coordinates of the Gauss integration points (see eq.1.136)
    :param element_spatial: 3x8
    contains the spatial nodal coordinates of the element

    :return:
    :value shape_func:  8x1 array - shape functions (interpolation) for the xi_vec locations
    :value grad_shape_func: 3x8 array - gradients of shape functions
    :value det_jac: the Jacobian of the mapping (3x3)
    """
    node_reference = np.array([[-1, 1, 1, -1, -1, 1, 1, -1],
                               [-1, -1, 1, 1, -1, -1, 1, 1],
                               [-1, -1, -1, -1, 1, 1, 1, 1]])
    nodes = node_reference.shape[1]

    quar = 0.125  # = 1/8
    shape_func, dxi_shape_func = np.zeros((8, 1)), np.zeros((3, 8))
    for node in range(nodes):
        nodal_xi, nodal_eta, nodal_zeta = [node_reference[c, node] for c in range(3)]
        # # shape function: N_I(\nodal_xi) = 1/8 * (1+\nodal_xi\xi_I)(..)(..)  ->
        # value of nodal shape function for the given \nodal_xi,\eta,\zeta (eq.1.136)
        lagrange_interpolants = np.array([1 + xi_vec[0] * nodal_xi, 1 + xi_vec[1] * nodal_eta,
                                          1 + xi_vec[2] * nodal_zeta])
        shape_func[node] = quar * lagrange_interpolants[0] * lagrange_interpolants[1] * lagrange_interpolants[2]

        # derivatives: \frac{ \partial N_I }{ \partial xi } = 1/8 * \xi_I * (1 + \eta\eta_I)*(1 + \zeta\zeta_I)
        dxi_shape_func[0, node] = quar * nodal_xi * lagrange_interpolants[1] * lagrange_interpolants[2]
        dxi_shape_func[1, node] = quar * nodal_eta * lagrange_interpolants[0] * lagrange_interpolants[2]
        dxi_shape_func[2, node] = quar * nodal_zeta * lagrange_interpolants[0] * lagrange_interpolants[1]

    # eq. 1.137: J = \Sum_1^8    x_I    \frac{ \partial N_I(\nodal_xi) } {\partial \nodal_xi },  element_spatial = x_I
    jacobian = dxi_shape_func @ element_spatial
    det_jac = np.linalg.det(jacobian)
    inv_jacob = np.linalg.inv(jacobian)
    grad_shape_func = inv_jacob @ dxi_shape_func  # eq. 1.138 \frac{ \partial N_I }{ \partial x_I }

    return [shape_func, grad_shape_func, det_jac]


def integrity_check(nodes_spatial: np.ndarray, element_connectivity: np.ndarray) -> float:
    r"""
    checks element connectivity and volume (element_interpolation fed write_file/ xi_vec = 0)
    file_write is for the output object handler (open file) - No Use
    :param nodes_spatial: spatial coordinates for element number_of_nodes
    :param element_connectivity: element connectivity matrix
    :return: the volume of the solid (as described by the element number_of_nodes)
    """
    eps = 1.0e-7
    number_of_elements = np.shape(element_connectivity)[0]
    volume = 0.0
    for element in range(number_of_elements):
        element_spatial = nodes_spatial[element_connectivity[element, :], :]
        _, _, det_jac = element_interpolation(np.r_[0, 0, 0], element_spatial)
        # det_jac is the determinant of the Jacobian \frac{ d x }{ d \xi }; the volume change is represented by the
        # jacobian \frac{ d x }{ d X } (material to spatial coordinates); from this you see that \xi plays the role
        # of the material coordinates in finite elements
        DVOL = 8 * det_jac  # det_jac comes from the sum over all number_of_nodes (via the matrix multiplication), this makes no sense
        if DVOL < eps:
            print("negative Jacobian \n Element connectivity:\n")
            print("{0}".format(element_connectivity[element, :]))
            print("\nnodal coordinates\n")
            print("{}".format(element_spatial))
            raise ValueError("Negative Jacobian")
        volume = volume + DVOL
    return volume
