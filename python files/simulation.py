r"""
runs the simulation for the body (mesh and material) defined in the 'materials' module;
"""

import numpy as np
import scipy.sparse

from materials.hyperelasticModel import MaterialHyperelastic
from materials.elasticModel import MaterialElastic
from elements.prettyprint import ndtotext

# globals - initialized later on
prob_dim, vec_size, neq, total_nodes = [0 for _ in range(4)]
displacement_vec, stress = [np.empty(0) for _ in range(2)]
body, load, boundary = None, None, None
write_to_file = False


def boundary_body_load(sim_type="linear"):
    # External forces [Node, DOF, Value]
    load = np.array([[5, 3, 500], [6, 3, 500], [7, 3, 500], [8, 3, 500]])
    # Prescribed displacements [Node, DOF, Value]
    boundary = np.array([
        [1, 1, 0], [1, 2, 0], [1, 3, 0],
                   [2, 2, 0], [2, 3, 0],
                              [3, 3, 0],
        [4, 1, 0],            [4, 3, 0]
    ])

    body_nonlinear = MaterialHyperelastic()
    body_linear = MaterialElastic()

    if sim_type == "linear":
        return body_linear, load, boundary
    elif sim_type == "nonlinear":
        return body_nonlinear, load, boundary


def init_finish(body):
    global prob_dim, vec_size, neq, total_nodes, displacement_vec, stress
    prob_dim = 3
    vec_size = 2 * prob_dim  # the reduced vector form for the symmetric tensors
    neq = body._nodes_spatial.size  # number of equations
    total_nodes = body._nodes_spatial.shape[0]
    displacement_vec = np.zeros(neq)
    stress = np.zeros((vec_size, total_nodes))

# ------------------------------------ parameter initialization finish


def boundary_update():
    dim_essential = boundary.shape[0]
    if dim_essential != 0:
        fixeddof = prob_dim * (boundary[:, 0] - 1) + boundary[:, 1]
        fixeddof = np.int_(fixeddof) - 1
        global_stiffness_matx[fixeddof, :] = np.zeros((dim_essential, neq))
        global_stiffness_matx[np.ix_(fixeddof, fixeddof)] = body.material_properties[0] * np.eye(dim_essential)
        force[fixeddof] = 0
        # this initialization of forces is for displacement-controlled simulation
        if iteration == 1:
            force[fixeddof] = body.material_properties[0]*boundary[:, 2]


def convergence_test():
    fixeddof = 3 * (boundary[:, 0] - 1) + boundary[:, 1]
    fixeddof = np.int_(fixeddof)
    fixeddof -= 1            # nodes have been indexed from 1 and on in the above
    alldof = np.r_[:neq]
    freedof = np.setdiff1d(alldof, fixeddof)
    return max(abs(force[freedof]))


def displacement_update():
    global displacement_vec
    displacement_vec += np.linalg.solve(global_stiffness_matx, force)


def load_update():
    if load.shape[0] > 0:
        loc = prob_dim * (load[:, 0] - 1) + load[:, 1]
        loc = np.int_(loc) - 1
        force[loc] += load[:, 2]


if __name__ == "__main__":
    # ******************************************************************************************************************
    # simulation:
    residual, iteration = 1.0e2, 0
    # body, load, boundary = boundary_body_load('linear')
    body, load, boundary = boundary_body_load('nonlinear')
    init_finish(body)

    while (residual > 1e-6) & (iteration < 60):
        iteration += 1
        # clear up everything - the only history (state) for the solid is its displacement
        global_stiffness_matx = scipy.sparse.csr_matrix((neq, neq)).toarray()
        force = scipy.sparse.csr_matrix((neq, 1)).toarray().flatten()

        global_stiffness_matx, force, strain_energy = body.material_response(global_stiffness_matx, force, stress,
                                                                      displacement_vec)

        load_update()
        boundary_update()
        displacement_update()

        if iteration > 1:
            residual = convergence_test()
        print(f'iteration: {iteration}\t, residual: {residual}')
        print(f'internal forces:\n {ndtotext(np.c_[np.arange(force.size)+1, force])}')

    print(f'current displacement:\n {ndtotext(np.copy(displacement_vec).reshape(total_nodes, 3))}')

    ## uncomment next line to get the constraint forces
    global_stiffness_matx, force, strain_energy = body.material_response(global_stiffness_matx, force, stress,
                                                                  displacement_vec)
    print(f'force: {ndtotext(np.c_[np.arange(force.size), force])}')
