import numpy as np


def mooney(deformation_grad: np.ndarray, strain_energy_params, calc_mat_stiff: bool) -> tuple:
    r"""
    calculates the 2nd Piola-Kirchhoff stress (vector) and material-stiffness (array) for materials described by the Mooney-Rivlin model
    (describes the strain-energy equation)
    :param deformation_grad - F = array [3x3]
    :param strain_energy_params: A10:float, A01:float, k:float - the polynomial coefficients for the Mooney-Rivlin
                                model in eq. 3.107 and k: being the bulk modulus for the dilatation part

    :return:
    :value stress_vec = 2nd Piola-Kirchhoff [S11, S22, S33, S12, S23, S13] (is symmetric)
     :value material_stiffness D = array [6x6]
    """
    A10, A01, k = strain_energy_params
    C = deformation_grad.T @ deformation_grad
    C1, C2, C3, C4, C5, C6 = C[0, 0], C[1, 1], C[2, 2], C[0, 1], C[1, 2], C[0, 2]
    I1 = C1 + C2 + C3
    I2 = C1 * C2 + C2 * C3 + C3 * C1 - C4 ** 2 - C5 ** 2 - C6 ** 2
    I3 = np.linalg.det(C)
    J3 = np.sqrt(I3)

    # derivatives write_file.r.t. Lagrange strain E
    I1E = 2 * np.array([1, 1, 1, 0, 0, 0]).T
    I2E = 2 * np.array([C2 + C3, C3 + C1, C1 + C2, -C4, -C5, -C6]).T
    I3E = 2 * np.array([C2 * C3 - C5 ** 2, C3 * C1 - C6 ** 2, C1 * C2 - C4 ** 2,
                        C5 * C6 - C3 * C4, C6 * C4 - C1 * C5, C4 * C5 - C2 * C6]).T

    # reduced invariants to construct the stress vector (see form of derivatives of invariants)
    mult1, mult2, mult3 = I3 ** (-1 / 3), (1 / 3) * I1 * I3 ** (-4 / 3), I3 ** (-2 / 3)
    mult4, mult5 = (2 / 3) * I2 * I3 ** (-5 / 3), (1 / 2) * I3 ** (-1 / 2)

    J1E = mult1 * I1E - mult2 * I3E
    J2E = mult3 * I2E - mult4 * I3E
    J3E = mult5 * I3E

    stress_vec = A10 * J1E + A01 * J2E + k * (J3 - 1.0) * J3E


    # calculation of the material stiffness
    material_stiffness = np.zeros((6, 6))
    if calc_mat_stiff:
        I2EE = np.array([
            [0., 4.0, 4.0, 0., 0., 0.],
            [4.0, 0., 4.0, 0., 0., 0.],
            [4.0, 4.0, 0., 0., 0., 0.],
            [0., 0., 0., -2.0, 0., 0.],
            [0., 0., 0., 0., -2.0, 0.],
            [0., 0., 0., 0., 0., -2.0],
        ])
        I3EE = np.array([
            [0., 4.0 * C3, 4.0 * C2, 0., -4.0 * C5, 0.],
            [4.0 * C3, 0., 4.0 * C1, 0., 0., -4.0 * C6],
            [4.0 * C2, 4.0 * C1, 0., -4.0 * C4, 0., 0.],
            [0., 0., -4.0 * C4, -2.0 * C3, 2 * C6, 2 * C5],
            [-4.0 * C5, 0., 0., 2.0 * C6, -2.0 * C1, 2.0 * C4],
            [0., -4.0 * C6, 0., 2.0 * C5, 2.0 * C4, -2.0 * C2]
        ])

        mmult1, mmult2, mmult3 = (2 / 3) * I3 ** (-1 / 2), (8 / 9) * I1 * I3 ** (-4 / 3), (1 / 3) * I1 * I3 ** (-4 / 3)
        mmult4, mmult5, mmult6 = (4 / 3) * I3 ** (-1 / 2), (8 / 9) * I2 * I3 ** (-5 / 3), I3 ** (-2 / 3)
        mmult7, mmult8, mmult9 = (2 / 3) * I2 * I3 ** (-5 / 3), I3 ** (-1 / 2), (1 / 2) * I3 ** (-1 / 2)

        J1EE = -mmult1 * (J1E[None].T @ J3E[None] + J3E[None].T @ J1E[None]) + mmult2 * (J3E[None].T @ J3E[None]) - mmult3 * I3EE
        J2EE = -mmult4 * (J2E[None].T @ J3E[None] + J3E[None].T @ J2E[None]) + mmult5 * (J3E[None].T @ J3E[None]) + mmult6 * I2EE - mmult7 * I3EE
        J3EE = -mmult8 * (J3E[None].T @ J3E[None]) + mmult9 * I3EE

        material_stiffness = A10 * J1EE + A01 * J2EE + k * (J3E[None].T @ J3E[None]) + k * (J3 - 1) * J3EE

    return stress_vec, material_stiffness


def cauchy(deformation_grad: np.ndarray, stress_2nd: np.ndarray) -> np.ndarray:
    r"""
    converts the 2nd Piola-Kirchhoff stress to Cauchy stress (eq.3.47):
    $ \sigma = \frac{1}{J} FSF^T $
    :param deformation_grad - F = array [3x3]
    :param stress_2nd - array[1x6] (stress in vector form)

    :return:
    :value (Cauchy) stress_vec - array[1x6]
    """
    stress_tensor = np.array([
        [stress_2nd[0], stress_2nd[3], stress_2nd[5]],
        [stress_2nd[3], stress_2nd[1], stress_2nd[4]],
        [stress_2nd[5], stress_2nd[4], stress_2nd[2]],
    ])
    Jinv = 1/np.linalg.det(deformation_grad)
    stress_Cauchy = Jinv * deformation_grad @ stress_tensor @ deformation_grad.T

    return np.array([
        stress_Cauchy[0,0], stress_Cauchy[1,1], stress_Cauchy[2,2],
        stress_Cauchy[0,1], stress_Cauchy[1,2], stress_Cauchy[0,2]
    ]).T
