import numpy as np
from elements.interpolation import element_interpolation
from materials.constitutive_model import mooney, cauchy
from elements.prettyprint import ndtotext

class MaterialHyperelastic(object):
    def __init__(self):
        # Material properties for Mooney with near-incompressibility condition:
        self.material_properties = np.array([80.0, 20.0, 1.0e7]) #    {A01, A10, K}

        self._nodes_spatial = np.array([                                                         # {XYZ}
            [0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0],
            [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]
        ])
        # node connectivity matrix
        self.element_connectivity = np.r_[:8][np.newaxis, :]                                    # {LE}

    def material_response(self, global_stiffness_matx, force, _, displacement_vec) -> tuple:
        vec_size = 6  # vector size for the 3D vectorized descriptions of strain, stress
        nodes = 8     # self._nodes_spatial.shape[0]  # total number of nodes
        elements = self.element_connectivity.shape[0]  # {NE}
        equations = 24 # self._nodes_spatial.size  # {3D x 8nodes: 24}
        problem_dim = self._nodes_spatial.shape[1]  # {NDOF}
        A10, A01, k = self.material_properties
        strain_energy = 0

        # Gauss integration points
        gauss_locations = np.array([-0.57735026918963, 0.57735026918963])
        gauss_weights = np.array([1.00000000000000, 1.00000000000000])

        # loop over number_of_elements, compute K and F
        for element in range(elements):
            element_spatial = self._nodes_spatial[self.element_connectivity[element, :], :]

            # from the global displacement vector pick the number_of_nodes that correspond to the current element
            local_eq_mask = np.empty(equations, dtype=int)
            for node in range(nodes):
                dum = node * problem_dim
                local_eq_mask[dum:dum + 3] = np.arange((self.element_connectivity[element, node]) * problem_dim,
                                                       (self.element_connectivity[element, node]) * problem_dim + 3)

            # select only the relevant, to this element, displacements
            displacements = displacement_vec[local_eq_mask]
            displacements = displacements.reshape(problem_dim, nodes, order='F').copy()

            # loop over integration points | gpx: gauss point in x_direction
            number_integration_points = gauss_locations.shape[0]
            strain1 = np.zeros((vec_size, number_integration_points**problem_dim))
            strain2 = np.zeros((vec_size, number_integration_points ** problem_dim))
            stress_keep = np.zeros((vec_size, number_integration_points ** problem_dim))
            cauchy_stress_keep = np.zeros((vec_size, number_integration_points ** problem_dim))
            stress_by_strain = np.zeros((vec_size, number_integration_points ** problem_dim))

            for gpx in range(number_integration_points):
                for gpy in range(number_integration_points):
                    for gpz in range(number_integration_points):
                        _, d_shape_func, detJ = element_interpolation(
                            np.array([gauss_locations[gpx], gauss_locations[gpy], gauss_locations[gpz]]),
                            element_spatial)
                        omega_integral = gauss_weights[gpx] * gauss_weights[gpy] * gauss_weights[gpz] * detJ  # eq. 1.140+

                        deformation_grad = displacements @ d_shape_func.T + np.eye(3)  # eq. 3.139 + 3.137

                        stress_2nd, material_stiffness = mooney(deformation_grad, [A10, A01, k], True)

                        strain_disp_nonlin_mat = np.zeros((vec_size, equations))
                        strain_disp_lin_mat = np.zeros((vec_size + problem_dim,
                                                        equations))
                        F, dN = deformation_grad, d_shape_func
                        for node in range(nodes):
                            strain_disp_nonlin_mat[..., node * 3:node * 3 + 3] = np.array([
                                [dN[0, node] * F[0, 0], dN[0, node] * F[1, 0], dN[0, node] * F[2, 0]],
                                [dN[1, node] * F[0, 1], dN[1, node] * F[1, 1], dN[1, node] * F[2, 1]],
                                [dN[2, node] * F[0, 2], dN[2, node] * F[1, 2], dN[2, node] * F[2, 2]],

                                [
                                    dN[0, node] * F[0, 1] + dN[1, node] * F[0, 0],
                                    dN[0, node] * F[1, 1] + dN[1, node] * F[1, 0],
                                    dN[0, node] * F[2, 1] + dN[1, node] * F[2, 0]
                                ],
                                [
                                    dN[1, node] * F[0, 2] + dN[2, node] * F[0, 1],
                                    dN[1, node] * F[1, 2] + dN[2, node] * F[1, 1],
                                    dN[1, node] * F[2, 2] + dN[2, node] * F[2, 1]
                                ],
                                [
                                    dN[0, node] * F[0, 2] + dN[2, node] * F[0, 0],
                                    dN[0, node] * F[1, 2] + dN[2, node] * F[1, 0],
                                    dN[0, node] * F[2, 2] + dN[2, node] * F[2, 0],
                                ]
                            ])

                            strain_disp_lin_mat[..., node * 3:node * 3 + 3] = np.array([
                                [dN[0, node], 0., 0.],
                                [dN[1, node], 0., 0.],
                                [dN[2, node], 0., 0.],
                                [0., dN[0, node], 0.],
                                [0., dN[1, node], 0.],
                                [0., dN[2, node], 0.],
                                [0., 0., dN[0, node]],
                                [0., 0., dN[1, node]],
                                [0., 0., dN[2, node]]
                            ])

                        # Load is applied through the boundary conditions **on top of** the internal stresses
                        # calculated by the saved displacement (system state)
                        force[local_eq_mask] -= omega_integral * (
                                    strain_disp_nonlin_mat.T @ stress_2nd).flatten()  # eq. 2.34


                        integration_point = gpx * 4 + gpy * 2 + gpz  # inner to outer loop-counter

                        strain = strain_disp_nonlin_mat @ displacement_vec[local_eq_mask]
                        strain1[:, integration_point] = strain
                        si = 0.5*(F.T @ F - np.eye(3))
                        strain2[:, integration_point] = np.c_[si[0,0], si[1,1], si[2,2], 2*si[0,1], 2*si[1,2], 2*si[0,2]]
                        stress_keep[:, integration_point] = stress_2nd
                        cauchy_stress_keep[:, integration_point] = cauchy(F, stress_2nd)

                        stress_by_strain[:, integration_point] = stress_2nd* strain
                        strain_energy += omega_integral * stress_by_strain[:, integration_point]

                        # do \Int stress:strain d\Omega to calc the strain energy eq. 1.88

                        sig = np.array([
                            [stress_2nd[0], stress_2nd[3], stress_2nd[5]],
                            [stress_2nd[3], stress_2nd[1], stress_2nd[4]],
                            [stress_2nd[5], stress_2nd[4], stress_2nd[2]],
                        ])
                        sigma = np.kron(np.eye(3), sig)

                        bdb = strain_disp_nonlin_mat.T @ material_stiffness @ strain_disp_nonlin_mat + \
                            strain_disp_lin_mat.T @ sigma @ strain_disp_lin_mat
                        global_stiffness_matx[np.ix_(local_eq_mask, local_eq_mask)] += \
                            omega_integral * bdb

        ## use this to debug
        # integr_points_in_order = [0, 4, 6, 2, 1, 5, 7, 3]
        # print(f'displacement-vec: {displacement_vec}\n')
        # print(f'strain1 * 1e3: {ndtotext(strain1[:, integr_points_in_order] * 1e3)}\n')
        # print(f'stress-by-strain: {ndtotext(stress_by_strain[:, integr_points_in_order])}\n')
        # print(f'strain2 * 1e3: {ndtotext(strain2[:, integr_points_in_order] * 1e3)}\n')
        # print(f'Cauchy-stress: {ndtotext(cauchy_stress_keep[:, integr_points_in_order])}\n')
        # print(f'strain-energy: {ndtotext(strain_energy[None])}\n')
        # print(f'TOTAL-strain-energy: {np.sum(strain_energy)}\n')


        return global_stiffness_matx, force, strain_energy

