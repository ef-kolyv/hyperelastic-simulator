import numpy as np
from elements.interpolation import element_interpolation


class MaterialElastic(object):

    def __init__(self):
        # Material properties:
        # lambda, mu in GPa
        self.material_properties = np.array([110.747e3, 80.1938e3])  # [\lambda \mu] (Lame's coef)   {PROP}
        lamda, mu = self.material_properties
        self.elasticity_tensor = np.array([
            [lamda + 2 * mu, lamda, lamda, 0, 0, 0],
            [lamda, lamda + 2 * mu, lamda, 0, 0, 0],
            [lamda, lamda, lamda + 2 * mu, 0, 0, 0],
            [0, 0, 0, mu, 0, 0],
            [0, 0, 0, 0, mu, 0],
            [0, 0, 0, 0, 0, mu]
        ])
        # Object description:
        # spatial coordinates (x,y,z) of the element's number_of_nodes: 1x element 8x number_of_nodes
        # if node coordinates are in sequence, the connectivity array _element_connectivity is just this sequence 0->7
        self._nodes_spatial = np.array([  # {XYZ}
            [0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0],
            [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]
        ])
        # node connectivity matrix
        # 2-dim matrix: contains rows of the connectivity of nodes for each element
        self._element_connectivity = np.r_[:8][np.newaxis, :]  # {LE}


    def material_response(self, global_stiffness_matx, force, stress, displacement_vec):
        r"""
        input: displacements 
        calculates: strain, stress, force (residual) [global], global_stiffness_matrix {k} [global]
        returns: force, global_stiffness_matrix
        """

        v = 6  # hard coding the standard vector size for the vectorized descriptions of strain, stress
        nodes = 8
        elements = self._element_connectivity.shape[0]  # {NE}
        equations = 24
        problem_dim = self._nodes_spatial.shape[1]  # {NDOF}

        # Gauss integration points
        gauss_locations = np.array([-0.57735026918963, 0.57735026918963])
        gauss_weights = np.array([1.00000000000000, 1.00000000000000])

        # loop over number_of_elements, compute K and F
        for element in range(elements):
            element_spatial = self._nodes_spatial[self._element_connectivity[element, :], :]

            # from the global displacement vector pick the number_of_nodes that correspond to the current element
            local_eq_mask = np.empty(equations, dtype=int)
            for node in range(nodes):
                dum = node * problem_dim
                local_eq_mask[dum:dum + 3] = np.arange((self._element_connectivity[element, node]) * problem_dim,
                                                       (self._element_connectivity[element, node]) * problem_dim + 3)

            # select only the relevant, to this element, displacements
            displacements = displacement_vec[local_eq_mask]

            displacements = displacements.reshape(problem_dim, nodes, order='F').copy()

            # loop over integration points | gpx: gauss integration_point in x_direction
            number_integration_points = gauss_locations.shape[0]
            for gpx in range(number_integration_points):
                for gpy in range(number_integration_points):
                    for gpz in range(number_integration_points):

                        _, d_shape_func, detJ = element_interpolation(
                            np.array([gauss_locations[gpx], gauss_locations[gpy], gauss_locations[gpz]]),
                            element_spatial)
                        omega_integral = gauss_weights[gpx] * gauss_weights[gpy] * gauss_weights[
                            gpz] * detJ  # eq. 1.140+

                        # strain
                        strain_tensor = displacements @ d_shape_func.T  # eq. 1.139

                        strain_vec = np.array([[strain_tensor[0, 0], strain_tensor[1, 1], strain_tensor[2, 2],
                                                strain_tensor[0, 1] + strain_tensor[1, 0],
                                                strain_tensor[1, 2] + strain_tensor[2, 1],
                                                strain_tensor[0, 2] + strain_tensor[2, 0]]
                                               ]).T

                        stress_temp = self.elasticity_tensor @ strain_vec
                        integration_point = gpx * 4 + gpy * 2 + gpz  # inner to outer loop-counter

                        if True:  # save_stress
                            # the stress saved here correspond to the state of the material around each integration
                            # point (follows the location at which the shape functions have been evaluated)
                            stress[:, integration_point] = stress_temp.flatten()

                        strain_disp_mat = np.zeros((v, equations))  # eq. 1.139
                        for node in range(nodes):
                            strain_disp_mat[..., node * 3:node * 3 + 3] = np.array([
                                [d_shape_func[0, node], 0, 0],
                                [0, d_shape_func[1, node], 0],
                                [0, 0, d_shape_func[2, node]],
                                [d_shape_func[1, node], d_shape_func[0, node], 0],
                                [0, d_shape_func[2, node], d_shape_func[1, node]],
                                [d_shape_func[2, node], 0, d_shape_func[0, node]]
                            ])

                        # here you are using the augmented form of the displacement_strain matrix <|. p.79 (bottom)
                        # [B] = [ B1, B2, .., B_number_of_nodes ]
                        # by doing the update of `force` in this loop you are essentially doing ONE of
                        # the summation in eq.1.141. you see, $\alpha$ is dum_displacement0 scalar (after both summations and the
                        # integration). What you have done up to here is:
                        # 1. calculated each of the sums_over_the_integration points (<|1.145) (you should also be multiplying the result for each of the axis - not done, that's why you get 1 value per Dof)
                        # 2. done one of the sums_over_the_nodes (by using the augmented [B] trick) (you should be doing one more summation over the nodes <|eq 1.141) - that's why you get 1 value per node)
                        # 3. the result is that force is now dum_displacement0 matrix of dim: node x dof (should have been dum_displacement0 scalar in the end) - but forces correspond to nodes (the fact that you have evaluated the shape functions
                        # on the integration points only means that you have done the evaluation correctly (<| eq. 1.144)
                        force[local_eq_mask] -= omega_integral * (strain_disp_mat.T @ stress_temp).flatten()  # eq.2.34
                        # the result here is

                        # global stiffness matrix {k}
                        if True:  # calc_global_stiffness:
                            bdb = strain_disp_mat.T @ self.elasticity_tensor @ strain_disp_mat
                            # {K} is 24x24; by indexing write_file/ [loc.., loc..] you get the rows indicated by loc.. at the
                            # columns indicated by loc.. --> 24x1 vector; this is different in Matlab notation
                            # use np.ix_ or just "global_stiffness_matx[local_eq_mask]"
                            global_stiffness_matx[np.ix_(local_eq_mask, local_eq_mask)] += omega_integral * bdb

        return global_stiffness_matx, force, stress
